﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="18008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)`!!!*Q(C=\&gt;7^&lt;3*2&amp;-8RMSM(G^,"CA\M5Q%3%;%F.H!"NQ632^9Y&gt;8:&lt;I!6;O#U1O!&amp;;90`TZMFA7\+$`:!N?99XQ(FP\PMR8UBN_3&amp;&gt;[0D?ID?8K&lt;^NRUV\4:PTHF-[&gt;5XPZ`X0"RV@^2^@&lt;M\\4`/@%-`[X`,`U=``7L[74\`=PHM4`00\Y&amp;O\%WF&amp;3VL10.X.26\E26\E26\E3:\E3:\E3:\E12\E12\E12\E*D?ZS5VO=P@HD=B&amp;,H+Z04O+S9O*CK*&amp;A7)Q&amp;"7&lt;QF.Y#E`BY;M+4_%J0)7H]$"%B;@Q&amp;*\#5XD9497H]"3?QF.Y+.5EV6:S0)7(]EI]C3@R**\%QZ2+0!EAG3QJH"3"I;1T_:"Y%E`CY;-34_**0)EH]&gt;#NR*.Y%E`C34TMUIZ+.MWYEO/BD!*0Y!E]A3@Q5&amp;K"*`!%HM!4?*B/A3@Q")BAQK!Y"!5\"1/#,Y%H]0#GQ".Y!E`A#4RUN4-5\=C-GH%FRW-]RG-]RG-]F*$R')`R')`R5&amp;&lt;'9TT'9TT'QV1S(O-R(A-RET+^4$'TIRFE!O0BV;Y7N\/54?+WKP]6N1N6`1*5P\$5,RDV#U(^"+O@/054IH[AV1_A_I&amp;2`](K0U1&gt;K$[R?E(VA4KQX&gt;.WN#VNI'VI;^K3.K@.RFX`]M$$Y;$^@K`&gt;&lt;K@N&gt;KNB',4:&lt;,2?L\6=,D7@TT7&lt;T:Y?!T^:HRY)UX0JFM^X6^?0^[O&lt;R&lt;$[N&lt;B@83`O,B]79`Y@`J]`Q,.2XX5_"]@I.WBV/\!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">402685952</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="NI_IconEditor" Type="Str">49 56 48 48 56 48 49 49 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 4 45 1 100 0 0 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 3 188 0 0 0 0 0 0 0 0 0 0 3 162 0 40 0 0 3 156 0 0 3 96 0 0 0 0 0 9 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 255 255 255 255 0 0 0 6 66 97 110 110 101 114 100 1 0 0 0 0 0 5 67 111 117 110 116 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 8 1 1
</Property>
	<Item Name="Public API" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Arguments" Type="Folder">
			<Item Name="Request" Type="Folder">
				<Item Name="Stop Argument--cluster.ctl" Type="VI" URL="../Stop Argument--cluster.ctl"/>
				<Item Name="Get Module Execution Status Argument--cluster.ctl" Type="VI" URL="../Get Module Execution Status Argument--cluster.ctl"/>
				<Item Name="Show Panel Argument--cluster.ctl" Type="VI" URL="../Show Panel Argument--cluster.ctl"/>
				<Item Name="Hide Panel Argument--cluster.ctl" Type="VI" URL="../Hide Panel Argument--cluster.ctl"/>
				<Item Name="Show Diagram Argument--cluster.ctl" Type="VI" URL="../Show Diagram Argument--cluster.ctl"/>
				<Item Name="Increment Argument--cluster.ctl" Type="VI" URL="../Increment Argument--cluster.ctl"/>
				<Item Name="Decrement Argument--cluster.ctl" Type="VI" URL="../Decrement Argument--cluster.ctl"/>
			</Item>
			<Item Name="Broadcast" Type="Folder">
				<Item Name="Did Init Argument--cluster.ctl" Type="VI" URL="../Did Init Argument--cluster.ctl"/>
				<Item Name="Status Updated Argument--cluster.ctl" Type="VI" URL="../Status Updated Argument--cluster.ctl"/>
				<Item Name="Error Reported Argument--cluster.ctl" Type="VI" URL="../Error Reported Argument--cluster.ctl"/>
				<Item Name="Update Count Argument--cluster.ctl" Type="VI" URL="../Update Count Argument--cluster.ctl"/>
			</Item>
		</Item>
		<Item Name="Requests" Type="Folder">
			<Item Name="Show Panel.vi" Type="VI" URL="../Show Panel.vi"/>
			<Item Name="Hide Panel.vi" Type="VI" URL="../Hide Panel.vi"/>
			<Item Name="Stop Module.vi" Type="VI" URL="../Stop Module.vi"/>
			<Item Name="Get Module Execution Status.vi" Type="VI" URL="../Get Module Execution Status.vi"/>
			<Item Name="Show Diagram.vi" Type="VI" URL="../Show Diagram.vi"/>
			<Item Name="Increment.vi" Type="VI" URL="../Increment.vi"/>
			<Item Name="Decrement.vi" Type="VI" URL="../Decrement.vi"/>
		</Item>
		<Item Name="Start Module.vi" Type="VI" URL="../Start Module.vi"/>
		<Item Name="Synchronize Module Events.vi" Type="VI" URL="../Synchronize Module Events.vi"/>
		<Item Name="Obtain Broadcast Events for Registration.vi" Type="VI" URL="../Obtain Broadcast Events for Registration.vi"/>
	</Item>
	<Item Name="Broadcasts" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Broadcast Events--cluster.ctl" Type="VI" URL="../Broadcast Events--cluster.ctl"/>
		<Item Name="Obtain Broadcast Events.vi" Type="VI" URL="../Obtain Broadcast Events.vi"/>
		<Item Name="Destroy Broadcast Events.vi" Type="VI" URL="../Destroy Broadcast Events.vi"/>
		<Item Name="Module Did Init.vi" Type="VI" URL="../Module Did Init.vi"/>
		<Item Name="Status Updated.vi" Type="VI" URL="../Status Updated.vi"/>
		<Item Name="Error Reported.vi" Type="VI" URL="../Error Reported.vi"/>
		<Item Name="Module Did Stop.vi" Type="VI" URL="../Module Did Stop.vi"/>
		<Item Name="Update Module Execution Status.vi" Type="VI" URL="../Update Module Execution Status.vi"/>
		<Item Name="Update Count.vi" Type="VI" URL="../Update Count.vi"/>
	</Item>
	<Item Name="Requests" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Request Events--cluster.ctl" Type="VI" URL="../Request Events--cluster.ctl"/>
		<Item Name="Obtain Request Events.vi" Type="VI" URL="../Obtain Request Events.vi"/>
		<Item Name="Destroy Request Events.vi" Type="VI" URL="../Destroy Request Events.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Init Module.vi" Type="VI" URL="../Init Module.vi"/>
		<Item Name="Handle Exit.vi" Type="VI" URL="../Handle Exit.vi"/>
		<Item Name="Close Module.vi" Type="VI" URL="../Close Module.vi"/>
		<Item Name="Module Data--cluster.ctl" Type="VI" URL="../Module Data--cluster.ctl"/>
		<Item Name="Module Name--constant.vi" Type="VI" URL="../Module Name--constant.vi"/>
		<Item Name="Module Timeout--constant.vi" Type="VI" URL="../Module Timeout--constant.vi"/>
		<Item Name="Module Not Running--error.vi" Type="VI" URL="../Module Not Running--error.vi"/>
		<Item Name="Module Not Synced--error.vi" Type="VI" URL="../Module Not Synced--error.vi"/>
		<Item Name="Module Not Stopped--error.vi" Type="VI" URL="../Module Not Stopped--error.vi"/>
		<Item Name="Get Module Main VI Information.vi" Type="VI" URL="../Get Module Main VI Information.vi"/>
		<Item Name="Open VI Panel.vi" Type="VI" URL="../Open VI Panel.vi"/>
		<Item Name="Hide VI Panel.vi" Type="VI" URL="../Hide VI Panel.vi"/>
	</Item>
	<Item Name="Module Sync" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Destroy Sync Refnums.vi" Type="VI" URL="../Destroy Sync Refnums.vi"/>
		<Item Name="Get Sync Refnums.vi" Type="VI" URL="../Get Sync Refnums.vi"/>
		<Item Name="Synchronize Caller Events.vi" Type="VI" URL="../Synchronize Caller Events.vi"/>
		<Item Name="Wait on Event Sync.vi" Type="VI" URL="../Wait on Event Sync.vi"/>
		<Item Name="Wait on Module Sync.vi" Type="VI" URL="../Wait on Module Sync.vi"/>
	</Item>
	<Item Name="Main.vi" Type="VI" URL="../Main.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</Library>
