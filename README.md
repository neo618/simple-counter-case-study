<h1>Description</h1>

This is a Case Study investigating the differences in some of the major design patterns.  The design patterns currently included are:
<ol><li>State Machine
<li>Event Handler
<li>Master/Slave
<li>Producer/Consumer
<li>Queued Message Handler (QMH)
<li>Functional Global Variable (FGV)
<li>Delacor Queued Message Handler (DQMH)
<li>Object-Oriented Queued Message Handler (OO QMH)
<li>Actor Framework
<li>QControl</ol>

<h1>LabVIEW Version</h1>

This code is currently published in LabVIEW 2018.

<h1>Build Instructions</h1>

This is distributed as source and not intended for a final product.

<h1>Installation Guide</h1>

This is distributed as source and not intended for a final product.

<h1>Execution</h1>
See documentation in the LabVIEW Community Wiki at: https://labview.wikia.com/

<h1>Support</h1>
Submit Issues or Merge Requests through GitLab.